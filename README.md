
# Project Name
![GitHub license](https://img.shields.io/github/license/Naereen/StrapDown.js.svg)
![Linux](https://svgshare.com/i/Zhy.svg) ![macOS](https://badgen.net/badge/os/mac%20os%20%20X%2010.11/green?icon=apple) ![Windows](https://badgen.net/badge/os/windows%2010/green?icon=windows)

![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg) ![forthebadge](https://forthebadge.com/images/badges/made-with-vue.svg) ![forthebadge](https://forthebadge.com/images/badges/made-with-java.svg)


Describe **shortly** in a few line on what is the repository about you can also add some graphical content like badges
1. See here list of available badges
2. [And some more designed badges](https://forthebadge.com/)
3. You can create your own badge here
4. you can also dynamically connect badges with environnement variable like pipeline status [see here how to proceed](https://medium.com/@iffi33/adding-custom-badges-to-gitlab-a9af8e3f3569)

## Table of contents
[[_TOC_]]

## Value and Key Feature

Describe In more specific ways what is the value comming out from the repository

 - you can also use 
 - bullets 
 - to exhausitively 
 - list the key features
 - provided by your project
 
You can also set the next steps of developpment with a Checkboxed roadmap
- [x] Next feature 1
- [x] Next feature 2
	- [x] Next feature 2.1
	- [ ] Next feature 2.2
- [ ] Next feature 3

## Getting started

### Deploy / install
Describe here how to install and deploy your code from a user / maintainer point of view
You can add some table if you want 

| Task           | URL | Comment   | 
|----------------|---------------|---------------|
| Install IDE | [VS code](https://code.visualstudio.com/download) | Check the rigth version for X64 environnement  |
| Install Docker | [Docker](https://docs.docker.com/desktop/windows/wsl/) | :no_entry: Do not use Docker Desktop application, use WSL2 instead |




### Use
Describe here the process to use the value from your code repository

    If some command line are required put it inside a code bracket

## Contribute
### Prerequiste
Describe here the required environnement to contribute to the project
you can use badges list or whatever you want to explained required dependencies


### Configuration rules

Define here the configuration rules for the project.

![enter image description here](https://i.imgur.com/TKUplcQ.png)

You can set up some extra infos
| Branch |Use| Rules |
|--|--|--|
| Main |Branch for production Use|<ul><li>protected branch</li><li>only by merge request</li><li>only from Preprod</li><li>Run every time after new commit on Preprod and test succeed</li></ul>|
|Preprod | For Test in Production like condition |<ul><li>protected branch</li><li>only by merge request</li><li>only from Dev</li><li>Run every week</li></ul> 
|Dev | main contribution branch | <ul><li>All feature and bug branches start from Dev</li><li>All Feature and bug branches merge on Dev</li><li>Only by merge request</li></ul>

 - Feature branches should allways respect following syntax

````
feature<featureid>#<short feature description>
````

 - Bug branches should always respect following syntax

````
bug<bugid>#<short bug description>
````


### Setup development environnement

Describe here how to setup developpment environment, required depencies, and all steps to reproduce to be able to start contribution

## Contact

for question about the repository you can ask

Head of Scientific Topic : @florian.pereme

Research Project manager: @florian.pereme



## Other 

### LaTeX

You can render LaTeX mathematical expressions using [KaTeX](https://khan.github.io/KaTeX/):

$`\sum_{i=0}^n i^2 = \frac{(n^2+n)(2n+1)}{6}`$

> You can find more information about **LaTeX** mathematical expressions [here](http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference).


### UML diagrams

You can render UML diagrams using [Mermaid](https://mermaidjs.github.io/). For example, this will produce a sequence diagram:

```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```
And this will produce a sequence chart:


```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```





### Gitlab flavored Markdown
The full capability of Gitlab markdown are available [here](https://docs.gitlab.com/ee/user/markdown.html) 

Some power online markdown
